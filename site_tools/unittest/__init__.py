"""
SCons tools - unittest
"""

import os

from fsops import ch_filename_ext, ch_dirpath, ch_target_filenode, prefix_filenode_name
from osops import is_windows
from SCons.Script import *

__author__ = "jtran"
__version__ = "2.1.0"

UNITTEST_FILENAME_PREFIX = "test_"
MOCK_FILENAME_PREFIX = "mock_"

SELF_DIRPATH = os.path.dirname(__file__)
UNITTEST_RUNNER_PY = os.path.join(SELF_DIRPATH, "unittest_runner.py")
UNITTEST_RUNNER_BATCH_PY = os.path.join(SELF_DIRPATH, "unittest_runner_batch.py")
UNITTEST_MOCKER_PY = os.path.join(SELF_DIRPATH, "mocker", "mocker.py")

UNITTEST_OBJ_DIRNAME = "obj"
MOCK_OBJ_DIRNAME = "obj"
UNITTEST_RESULT_DIRNAME = "results"


"""
SCons tools functions
"""


def generate(env):
    env.AddMethod(build_method, "Build")
    env.AddMethod(run_method, "Run")
    env.AddMethod(run_batch_method, "RunBatch")
    env.AddMethod(mock_method, "Mock")


def exists():
    return True


"""
Environment functions
"""


def build_method(self, target_dir, sources, mocks=None, source_repo=None):
    """
    SCons method
        - Compile all unit test sources
        - Compile all mock sources
        - Produce executable unit test files

    :param target_dir: Target directory node (AKA destination directory node) (Dir)
    :param sources: A list of unit test source file nodes (list of File or list of Dir)
    :param mocks: A list of mock file nodes (list of File or list of Dir)
    :param source_repo: A list of potential source files under test (list of File)
    :return: Tuple(
        A list of executables (List of File)
        A list of object file nodes compiled from sources (not mocks) for unit testing (list of File)
        A list of object file nodes compiled from mock sources for unit testing (list of File)
    )
    """
    target_dir = Dir(target_dir)
    sources = normalize_source_list(sources, "*.c*")
    mocks = [] if (mocks is None) else normalize_source_list(mocks, "*.h*")
    source_repo = [] if (source_repo is None) else list(map(File, source_repo))



    # Build all mocks
    mock_obj_filenodes = []
    for mock_filenode in mocks:
        target_mock_filenode = ch_target_filenode(filenode=mock_filenode, target_dirnode=target_dir.Dir(MOCK_OBJ_DIRNAME), ext="o")
        objs = self.Object(target=target_mock_filenode, source=mock_filenode)
        mock_obj_filenodes.extend(objs)

    # For each unit test source, compile and produce executable
    all_ut_obj_filenodes = []
    ut_exe_filenodes = []
    for ut_filenode in sources:
        target_ut_filenode = ch_target_filenode(filenode=ut_filenode, target_dirnode=target_dir.Dir(UNITTEST_OBJ_DIRNAME), ext="o")
        objs = self.Object(target=target_ut_filenode, source=ut_filenode)
        ut_obj_filenode = Flatten(objs)[0]
        all_ut_obj_filenodes.append(ut_obj_filenode)

        # For each unit test source, compile an executable binary
        exe_filenode = ch_target_filenode(filenode=ut_filenode, target_dirnode=target_dir, ext=("exe" if is_windows() else None))
        filtered_mock_obj_filenodes = filter_mock_objs(mock_obj_filenodes, ut_obj_filenode)  # Filter mocks; avoid compiling both source file under test and its mock counterpart (if any)
        exes = self.Program(target=exe_filenode, source=([ut_obj_filenode] + filtered_mock_obj_filenodes))
        ut_exe_filenodes.extend(exes)

    return ut_exe_filenodes, all_ut_obj_filenodes, mock_obj_filenodes


def mock_method(self, target_dir, sources):
    """
    Generate mock source files
    :param target_dir: Target directory node (AKA destination directory node) (Dir)
    :param sources: A list of header files to mock (list of File or list of Dir)
    :return: A list of generated mock C source file nodes (list of File)
    """
    target_dir = Dir(target_dir)
    sources = normalize_source_list(sources, "*.h*")

    cmd_fmt = "python {mock_py} -o $TARGET -i $SOURCE {include_paths_args} {defines_args}"
    targets = []
    for source_filenode in sources:
        _, source_ext = os.path.splitext(source_filenode.name)
        source_ext = source_ext.lstrip(".")  # Strip '.' in file extension (i.e. ".txt" -> "txt")
        source_ext = "c{}".format(source_ext[1:])  # Change first letter of file extension to 'c' (i.e. "h" -> "c" OR "hpp" -> "cpp")
        target_filenode = ch_target_filenode(filenode=source_filenode, target_dirnode=target_dir, ext=source_ext)
        target_filenode = prefix_filenode_name(filenode=target_filenode, prefix=MOCK_FILENAME_PREFIX)
        include_paths_args = list(map(lambda dirnode: "-I{}".format(Dir(dirnode).abspath), self.get("CPPPATH", [])))
        defines_args = list(map(lambda string: "-D{}".format(string), self.get("CPPDEFINES", [])))
        cmd = cmd_fmt.format(
            mock_py=UNITTEST_MOCKER_PY,
            include_paths_args=" ".join(include_paths_args),
            defines_args=" ".join(defines_args)
        )
        _ = self.Command(target=target_filenode, source=source_filenode, action=[cmd])
        targets.append(target_filenode)

    return targets


def run_method(self, source, verbose=False):
    source = list(map(File, source))

    result_filenodes = []
    for source_filenode in source:
        result_filenode = ch_target_filenode(filenode=source_filenode, target_dirnode=source_filenode.dir.Dir(UNITTEST_RESULT_DIRNAME), ext="txt")
        result_filenodes.append(result_filenode)

        cmd = "python {} -i $SOURCE -o $TARGET".format(UNITTEST_RUNNER_PY)
        if verbose:
            cmd = "{} --verbose".format(cmd)
        _ = self.Command(target=result_filenode, source=source_filenode, action=[cmd])
    return result_filenodes


def run_batch_method(self, source, verbose=False):
    source = list(map(File, source))

    result_filenodes = []
    for source_filenode in source:
        result_filenode = ch_target_filenode(filenode=source_filenode, target_dirnode=source_filenode.dir.Dir(UNITTEST_RESULT_DIRNAME), ext="txt")
        result_filenodes.append(result_filenode)

    input_args = " ".join(map(lambda filenode: "-i {}".format(filenode.abspath), source))
    output_args = " ".join(map(lambda filenode: "-o {}".format(filenode.abspath), result_filenodes))
    cmd = "python {} {} {}".format(UNITTEST_RUNNER_BATCH_PY, input_args, output_args)
    if verbose:
        cmd = "{} --verbose".format(cmd)
    _ = self.Command(target=result_filenode, source=source_filenode, action=[cmd])
    return result_filenodes


"""
Helpers
"""


def synthesize(ut_source, source_repo):
    """
    For a given unit test source file where the file name contains UNITTEST_FILENAME_PREFIX,
    find the counterpart source file pair (the source file under test)
    :param ut_source: Unit test source file node (File)
    :param source_repo: A list of source file nodes (list of File)
    :return: A list of unit test source file node and the counterpart source file under test (list of File)
    :raise: AssertionError
    """
    ut_source = File(ut_source)
    source_repo = list(map(File, source_repo))
    
    added_source_filenodes = []
    if ut_source.name.startswith(UNITTEST_FILENAME_PREFIX):
        target_source_filename = ut_source.name.replace(UNITTEST_FILENAME_PREFIX, "")  # test_filename.c -> filename.c
        for source_filenode in source_repo:
            if target_source_filename == source_filenode.name:
                added_source_filenodes.append(source_filenode)
                break
        else:
            raise AssertionError("Unable to find {}; needed by {}".format(target_source_filename, ut_source.abspath))

    sources = []
    sources.append(ut_source)
    sources.extend(added_source_filenodes)
    return sources


def filter_mock_objs(mock_objects, ut_object):
    """
    For a given unit test object file where the file name contains UNITTEST_FILENAME_PREFIX,
    exclude the counterpart mock object file where a mock source file name contains MOCK_FILENAME_PREFIX
    :param mock_sources: A list of mock source files nodes (list of File)
    :param ut_source: Unit test source file node (File)
    :return: A list of filtered mock source file nodes (list of File)
    """
    mock_objects = list(map(File, mock_objects))
    ut_object = File(ut_object)

    filtered_mock_objects = []
    if ut_object.name.startswith(UNITTEST_FILENAME_PREFIX):
        ut_source_filename = ut_object.name.replace(UNITTEST_FILENAME_PREFIX, "")
        for mock_object in mock_objects:
            if not mock_object.name.startswith(MOCK_FILENAME_PREFIX):
                continue
            mock_source_filename = mock_object.name.replace(MOCK_FILENAME_PREFIX, "")
            if mock_source_filename != ut_source_filename:
                filtered_mock_objects.append(mock_object)
    else:
        filtered_mock_objects = mock_objects

    return filtered_mock_objects


def normalize_source_list(sources, file_pattern=None):
    """
    :param sources: A list of unit test source file nodes (list of File or list of Dir)
    :param file_pattern: File pattern to glob against if source is a Directory node (str)
    :return A list of source file nodes (list of File)
    """
    sources_formatted = []
    for source in sources:
        try:
            sources_formatted.append(File(source))
        except:
            sources_formatted.append(Dir(source))

    source_filenodes = []
    for source in sources_formatted:
        if os.path.isdir(source.abspath):
            source_filenodes.extend(Glob(os.path.join(source.abspath, file_pattern if (file_pattern is not None) else "*")))
        else:  # isinstance(source, File)
            source_filenodes.append(source)

    return source_filenodes

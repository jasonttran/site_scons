"""
Mocker, wrapper for cgreen-mocker
"""

from argparse import ArgumentParser
import logging
import os
import subprocess
import sys

__author__ = "jtran"
__version__ = "1.0.0"

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

SELF_DIRPATH = os.path.dirname(__file__)
CGREEN_MOCKER_PY = os.path.join(SELF_DIRPATH, "cgreen-mocker.py")


class Mocker(object):
    def __init__(self, defines=None, include_paths=None, logger=None):
        self._defines = [] if (defines is None) else list(defines)
        self._include_paths = [] if (include_paths is None) else list(include_paths)
        self._logger = logger if (logger is not None) else logging.getLogger()

    def generate(self, header_filepath, output_filepath=None):
        if output_filepath is None:
            basename, ext = os.path.splitext(os.path.basename(header_filepath))
            output_filename = "{}.mock".format(basename)
            output_filepath = os.path.join(SELF_DIRPATH, output_filename)

        cli_args = []
        for define in self._defines:
            cli_args.append("-D{}".format(define))
        for include_path in self._include_paths:
            cli_args.append("-I{}".format(include_path))
        cli_args.append(header_filepath)

        error, stdout, stderr = self._execute_python_subprocess(py_filepath=CGREEN_MOCKER_PY, cli_args=cli_args)
        if not error:
            with open(output_filepath, "w") as file_obj:
                file_obj.write(stdout)
        else:
            self._logger.error(stdout)
            self._logger.error(stderr)

        return error


    """
    Private methods
    """
    @staticmethod
    def _execute_python_subprocess(py_filepath, cli_args=None):
        cmd = [
            "python",
            py_filepath,
        ]
        if cli_args is not None:
            cmd.extend(cli_args)
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        error = process.returncode
        return error, stdout, stderr


def get_args():
    arg_parser = ArgumentParser()
    arg_parser.add_argument(
        "-i", "--input",
        required=True
    )
    arg_parser.add_argument(
        "-o", "--output",
        required=True
    )
    arg_parser.add_argument(
        "-D", "--define",
        action="append",
    )
    arg_parser.add_argument(
        "-I", "--include",
        action="append",
    )
    return arg_parser.parse_args()


def main():
    args = get_args()
    mocker = Mocker(defines=args.define, include_paths=args.include)
    error = mocker.generate(header_filepath=args.input, output_filepath=args.output)
    return error


if __name__ == "__main__":
    sys.exit(main())

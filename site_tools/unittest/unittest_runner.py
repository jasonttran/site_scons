"""
Unittest Runner - Manages unit test process
"""

from argparse import ArgumentParser
from collections import namedtuple
import os
import subprocess
import sys
import time

__author__ = "jtran"
__version__ = "2.0.0"

Unittest = namedtuple("Unittest", ["exe_filepath", "result_filepath"])


class UnittestRunner(object):
    def __init__(self, unittest, verbose=False):
        assert isinstance(unittest, Unittest)
        self._exe_filepath = unittest.exe_filepath
        self._result_filepath = unittest.result_filepath
        self._verbose = verbose

        assert os.path.isfile(self._exe_filepath)

        if (self._result_filepath is not None) and not os.path.isdir(os.path.dirname(self._result_filepath)):
            os.makedirs(os.path.dirname(self._result_filepath))

        self._error = None
        self._stdout = None
        self._stderr = None
        self._timestamp = None

    """
    Public methods
    """
    def run(self):
        if self._verbose:
            print "Executing [{}]".format(self._exe_filepath)

        error, stdout, stderr = self._execute_subprocess(self._exe_filepath)

        # Write results to standard out
        if self._verbose:
            print(stdout)
            print(stderr)

        # Write results to file
        if self._result_filepath is not None:
            with open(self._result_filepath, "w") as file_obj:
                file_obj.write("{}\n".format(stdout))
                file_obj.write("{}\n".format(stderr))

        self._error = error
        self._stdout = stdout
        self._stderr = stderr
        self._timestamp = time.time()

        return error

    """
    Private methods
    """
    @staticmethod
    def _execute_subprocess(exe_filepath):
        cmd = [
            exe_filepath,
        ]
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        return process.returncode, stdout, stderr

    """
    Accessors
    """
    @property
    def exe_filepath(self):
        return os.path.abspath(self._exe_filepath)

    @property
    def result_filepath(self):
        return os.path.abspath(self._result_filepath)

    """
    Accessors - process attributes
    """
    @property
    def error(self):
        return self._error

    @property
    def stdout(self):
        return self._stdout
    
    @property
    def stderr(self):
        return self._stderr

    @property
    def timestamp(self):
        return self._timestamp


def get_args():
    arg_parser = ArgumentParser()
    arg_parser.add_argument(
        "-i", "--input",
        type=str,
        required=True
    )
    arg_parser.add_argument(
        "-o", "--output",
        type=str,
        default=None
    )
    arg_parser.add_argument(
        "--verbose",
        action="store_true"
    )
    return arg_parser.parse_args()


def main():
    args = get_args()
    exe_filepath = args.input
    output_filepath = args.output

    new_unittest = Unittest(exe_filepath, output_filepath)
    unittest_runner = UnittestRunner(unittest=new_unittest, verbose=args.verbose)
    error = unittest_runner.run()
    return error


if __name__ == "__main__":
    sys.exit(main())
